﻿$userModule.component(
    'addUser', {
        templateUrl: 'views/userForm.html',
        controller: ['$scope','$window',
            function addUserController($scope, $window) {
                $scope.tags = [
                    { text: 'Telephone' },
                    { text: 'Fax' },
                    { text: 'Email' }
                ];
                $scope.loadTags = function () {
                    var lstTags = [{ text: 'Telephone' }, { text: 'Fax' }, {text: 'Email'}];
                    return lstTags;
                };
            }
        ]
    });